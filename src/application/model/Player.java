package application.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType (propOrder={"name", "team", "points", "rank", "userID", "clanID", "inactive"})
public class Player {
	private String name;
	private String team; 
	private String points; 
	private String rank;
	private String userID; 
	private String clanID;  
	private String inactive; 
	
	public Player() {
		this.name = ""; 
		this.team = ""; 
		this.points = "";  
		this.rank = ""; 
		this.userID = "";
		this.clanID = "";
		this.inactive = "";
	}
	
	public Player(String name, String team, String points, String rank,
			String userID, String clanID, String inactive) {
		this.name = ""; 
		this.team = ""; 
		this.points = "";  
		this.rank = ""; 
		this.userID = "";
		this.clanID = "";
		this.inactive = "";
	}
	
	// ******************** Getters ********************
	@XmlElement(name = "name") public String getName() {return name;}
	@XmlElement(name = "team") public String getTeam() {return team;}
	@XmlElement(name = "points") public String getPoints() {return points;}
	@XmlElement(name = "rank") public String getRank() {return rank;}
	@XmlElement(name = "userID") public String getUserID() {return userID;}
	@XmlElement(name = "clanID") public String getClanID() {return clanID;}
	@XmlElement(name = "inactive") public String getInactive() {return inactive;}
	// ******************** Setters ********************
	public void setName(String name) {this.name = name;}
	public void setTeam(String team) {this.team = team;}
	public void setPoints(String points) {this.points = points;}
	public void setRank(String rank) {this.rank = rank;}
	public void setUserID(String userID) {this.userID = userID;}
	public void setClanID(String clanID) {this.clanID = clanID;}
	public void setInactive(String inactive) {this.inactive = inactive;}
	
	public String toString() {
		return "Name: " + name + 
			   " Team: " + team + 
			   " Points: " + points +
			   " Rank: " + rank +
		       " User ID: " + userID +
		       " Clan ID: " + clanID +
		       " Inactive: " + inactive;
	}
}
