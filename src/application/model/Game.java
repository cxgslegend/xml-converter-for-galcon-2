package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import application.util.DateUtil;
import application.util.LocalDateTimeAdapter;

@XmlType (propOrder={"playerList", "gameID", "timestamp", "duration", "serverID", "serverName", "board", 
		"adminID", "tourneyID", "tourneyName", "hostingClanID", "hostingClanName", "events", 
		"klass", "custom"})
public class Game {
	private List<Player> playerList;
	private String gameID; 
	private LocalDateTime timestamp; 
	private String duration; 
	private String serverID; 
	private String serverName; 
	private String board; 
	private String adminID; 
	private String tourneyID; 
	private String tourneyName; 
	private String hostingClanID; 
	private String hostingClanName; 
	private String events;  
	private String klass; 
	private String custom;
	
	public Game() {
		this.playerList = new ArrayList<Player>();
		this.gameID = "";
		this.timestamp = LocalDateTime.of(2014, 10, 27, 0, 0, 0);
		this.duration = "";
		this.serverID = "";
		this.serverName = "";
		this.board = "";
		this.adminID = "";
		this.tourneyID = "";
		this.tourneyName = "";
		this.hostingClanID = "";
		this.hostingClanName = "";
		this.events = "";
		this.klass = "";
		this.custom = "";
	}
	
	public Game(List<Player> playerList, String gameID,
			LocalDateTime timestamp, String duration, String serverID, String serverName,
			String board, String admin, String tourneyID, String tourneyName,
			String hostingClanID, String hostingClanName, String events, String klass, 
			String custom) {
		this.playerList = playerList;
		this.gameID = "";
		this.timestamp = timestamp;
		this.duration = "";
		this.serverID = "";
		this.serverName = "";
		this.board = "";
		this.adminID = "";
		this.tourneyID = "";
		this.tourneyName = "";
		this.hostingClanID = "";
		this.hostingClanName = "";
		this.events = "";
		this.klass = "";
		this.custom = "";
	}
	// ******************** Getters ********************
	@XmlElement(name = "player") public List<Player> getPlayerList() {return playerList;}
	@XmlElement(name = "gameID") public String getGameID() {return gameID ;}
	@XmlElement(name = "timestamp") @XmlJavaTypeAdapter(LocalDateTimeAdapter.class) public LocalDateTime getTimestamp() {return timestamp ;}
	@XmlElement(name = "duration") public String getDuration() {return duration ;}
	@XmlElement(name = "serverID") public String getServerID() {return serverID ;}
	@XmlElement(name = "serverName") public String getServerName() {return serverName ;}
	@XmlElement(name = "board") public String getBoard() {return board ;}
	@XmlElement(name = "adminID") public String getAdminID() {return adminID ;}
	@XmlElement(name = "tourneyID") public String getTourneyID() {return tourneyID ;}
	@XmlElement(name = "tourneyName") public String getTourneyName() {return tourneyName ;}
	@XmlElement(name = "hostingClanID") public String getHostingClanID() {return hostingClanID ;}
	@XmlElement(name = "hostingClanName") public String getHostingClanName() {return hostingClanName ;}
	@XmlElement(name = "events") public String getEvents() {return events ;}
	@XmlElement(name = "klass") public String getKlass() {return klass ;}
	@XmlElement(name = "custom") public String getCustom() {return custom ;}
	// ******************** Setters ********************
	public void setPlayerList(List<Player> playerList) {this.playerList = playerList;}
	public void setGameID(String gameID) {this.gameID = gameID;}
	public void setTimestamp(LocalDateTime timestamp) {this.timestamp = timestamp;}
	public void setDuration(String duration) {this.duration = duration;}
	public void setServerID(String serverID) {this.serverID = serverID;}
	public void setServerName(String serverName) {this.serverName = serverName;}
	public void setBoard(String board) {this.board = board;}
	public void setAdminID(String adminID) {this.adminID = adminID;}
	public void setTourneyID(String tourneyID) {this.tourneyID = tourneyID;}
	public void setTourneyName(String tourneyName) {this.tourneyName = tourneyName;}
	public void setHostingClanID(String hostingClanID) {this.hostingClanID = hostingClanID;}
	public void setHostingClanName(String hostingClanName) {this.hostingClanName = hostingClanName;}
	public void setEvents(String events) {this.events = events;}
	public void setKlass(String klass) {this.klass = klass;}
	public void setCustom(String custom) {this.custom = custom;}

	public String toString() {
		return "Player List: " + playerList.toString() + 
			   " Game ID: " + gameID + 
			   " Timestamp: " + DateUtil.format(timestamp ) +
			   " Duration: " + duration +
		       " ServerID: " + serverID +
		       " Server Name: " + serverName  +
		       " Board: " + board +
		       " Admin ID: " + adminID +
		       " Tourney ID: " + tourneyID +
		       " Tourney Name: " + tourneyName +
		       " Hosting Clan ID: " + hostingClanID +
		       " Hosting Clan Name: " + hostingClanName +
		       " Events: " + events +
		       " Klass: " + klass +
		       " Custom: " + custom ;
	}
}
