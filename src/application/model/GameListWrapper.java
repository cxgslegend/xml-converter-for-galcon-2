package application.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType (propOrder={"games"})
@XmlRootElement(name = "games")
public class GameListWrapper {
    private List<Game> games;

    @XmlElement(name = "game")
    public List<Game> getGames() {
        return games;
    }

    public void setPersons(List<Game> games) {
        this.games = games;
    }
}