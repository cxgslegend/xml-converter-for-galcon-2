package application.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.stage.DirectoryChooser;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import application.Main;
import application.model.Game;
import application.model.GameListWrapper;
import application.model.Player;
import application.util.DateUtil;
import application.util.ResourceIO;

public class ApplicationOverviewController {
	@FXML
	private Button downloadAllLogFilesButton;
	@FXML
	private Button convertButton;

	private Main main;
	private File txtFilePath;
	private File XMLFilePath;
	private List<Game> gameData; 
	private ResourceIO resourceIO;
	
	@FXML
	private void initialize() {
	}

	public void setMainApp(Main main) {
		this.main = main;
	}

	@FXML
	void setXMLPath(ActionEvent event) {
		DirectoryChooser directoryChooser = new DirectoryChooser();
		XMLFilePath = directoryChooser.showDialog(main.getPrimaryStage());
	}

	@FXML
	void setLogPath(ActionEvent event) {
		DirectoryChooser directoryChooser = new DirectoryChooser();
		txtFilePath = directoryChooser.showDialog(main.getPrimaryStage());
		if(txtFilePath != null) {
			resourceIO = new ResourceIO(txtFilePath.getPath() + "/");
		}
	}

	@FXML
	void downloadAllLogFiles(ActionEvent event) {
		if(txtFilePath == null) {return;}
		new Thread(() -> {
			resourceIO.updateLogs();
		}).start();
	}

	@FXML
	void convert(ActionEvent event) {
		if(XMLFilePath == null || txtFilePath == null) {return;} 
		new Thread(() -> {
			buildXMLAndJSONFiles(txtFilePath, XMLFilePath);			
		}).start();
	}

	public void buildXMLAndJSONFiles(File txtFilePath, File XMLFilePath) {
		List<String> datesList = null;
		try {
			datesList = resourceIO.getDatesList();
		} 
		catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		
		for(String date: datesList) {
			if(resourceIO.checkIfResourceExists(txtFilePath.getPath() + "/" + date + ".txt")) {
				File txtFileLocation = new File(txtFilePath.getPath() + "/" + date + ".txt");
				File xmlFileLocation = new File(XMLFilePath.getPath() + "/" + date + ".xml");
				gameData = buildGameData(txtFileLocation);
				if(!resourceIO.checkIfResourceExists(XMLFilePath.getPath() + "/" + date + ".xml")) {
					saveGameDataToXML(xmlFileLocation);
				}
			}
		}
	}

	public List<Game> buildGameData(File filePath) {
		Scanner textFile = new Scanner(readFiles(filePath));
		List<Game> gameList = new ArrayList();;
		while(textFile.hasNextLine()) {
			List<Player> playerList = new ArrayList();
			String lineOfGameData = textFile.nextLine();
			String [] gameData = lineOfGameData.split("\\t", -1);
			String [] playersData = gameData[0].split("\\|");
			Game tempGame = new Game();

			for (String player : playersData) {
				Player tempPlayer = new Player(); 
				String [] playerData = player.split(":", -1);
				for (int i = 0; i < playerData.length; i++) {
					switch (i) {
					case 0: tempPlayer.setName(playerData[i].equals("") ? "?" : playerData[i]); break;
					case 1: tempPlayer.setTeam(playerData[i].equals("") ? "?" : playerData[i]); break;
					case 2: tempPlayer.setPoints(playerData[i].equals("") ? "?" : playerData[i]); break;
					case 3: tempPlayer.setRank(playerData[i].equals("") ? "?" : playerData[i]); break;
					case 4: tempPlayer.setUserID(playerData[i].equals("") ? "?" : playerData[i]); break;
					case 5: tempPlayer.setClanID(playerData[i].equals("") ? "?" : playerData[i]); break;
					default: break;
					}
				}
				if(playerData.length == 7) {
					tempPlayer.setInactive(playerData[6].equals("") ? "?":playerData[6]);
				}
				else {
					tempPlayer.setInactive("?"); 
				}
				playerList.add(tempPlayer);
			}

			tempGame.setPlayerList(playerList);
			// data 0, id 1, ts 2 and 3, duration 4, server 5, name 6, board 7, [admin 8, tourney 9, clan 10], events 11, klass 12, custom 13
			for(int i = 1; i < gameData.length; i++) {
				switch (i) {
				case 1: tempGame.setGameID(gameData[i].equals("") ? "?":gameData[i]); break;
				case 2: 
					tempGame.setTimestamp(gameData[i].equals("") ? 
							LocalDateTime.of(2014, 10, 27, 0, 0, 0) : 
								DateUtil.parse(gameData[i]));
					break;
				case 3: tempGame.setDuration(gameData[i].equals("") ? "?":gameData[i]); break;
				case 4: tempGame.setServerID(gameData[i].equals("") ? "?":gameData[i]); break;
				case 5: tempGame.setServerName(gameData[i].equals("") ? "?":gameData[i]); break;
				case 6: tempGame.setBoard(gameData[i].equals("") ? "?":gameData[i]); break;
				case 7: tempGame.setAdminID(gameData[i].equals("") ? "?":gameData[i]); break;
				case 8:
					if(gameData[i].equals("")) {
						tempGame.setTourneyID("?"); 
						tempGame.setTourneyName("?"); 
					}
					else {
						String [] TourneyIdAndNameSeperator = gameData[i].split(":",-1);
						tempGame.setTourneyID(TourneyIdAndNameSeperator[0].equals("") ? "?":TourneyIdAndNameSeperator[0]); 
						tempGame.setTourneyName(TourneyIdAndNameSeperator[1].equals("") ? "?":TourneyIdAndNameSeperator[1]);
					}
					break;
				case 9:
					if(gameData[i].equals("")) {
						tempGame.setHostingClanID("?");
						tempGame.setHostingClanName("?");	
					}
					else {
						String [] clanIdAndNameSeperator = gameData[i].split(":",-1);
						tempGame.setHostingClanID(clanIdAndNameSeperator[0].equals("") ? "?" : clanIdAndNameSeperator[0]); 
						tempGame.setHostingClanName(clanIdAndNameSeperator[1].equals("") ? "?" : clanIdAndNameSeperator[1]);
					}
					break;
				case 10: tempGame.setEvents(gameData[i].equals("") ? "?":gameData[i]); break;
				case 11: tempGame.setKlass(gameData[i].equals("") ? "?":gameData[i]); break;
				case 12: tempGame.setCustom(gameData[i].equals("") ? "?":gameData[i]); break;
				default: break;
				}
			}
			gameList.add(tempGame);
		}

		return gameList;
	}

	public String readFiles(File filePath) {
		try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}

			return sb.toString();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	public void saveGameDataToXML(File filePath) {
		try {
			JAXBContext context = JAXBContext.newInstance(GameListWrapper.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			GameListWrapper wrapper = new GameListWrapper();
			wrapper.setPersons(gameData);

			m.marshal(wrapper, filePath);	        
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Could not save data");
			alert.setContentText("Could not save data to file:\n" + filePath.getPath());

			alert.showAndWait();
		}
	}
}
