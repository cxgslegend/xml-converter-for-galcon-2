package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import application.controller.ApplicationOverviewController;

public class Main extends Application {
	private Stage stage; 
	private BorderPane rootPane;

	public void start(Stage primaryStage) {
		try {
			stage = primaryStage;
			stage.setTitle("XML Converter");
			initRootLayout();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public Stage getPrimaryStage() {
		return stage; 
	}

    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/ApplicationOverview.fxml"));
            rootPane = (BorderPane) loader.load();

            Screen screen = Screen.getPrimary();
            Rectangle2D screenDimension = screen.getVisualBounds();
            Scene scene = new Scene(rootPane, screenDimension.getWidth()/1.2, screenDimension.getHeight()/1.2);
			scene.getStylesheets().add(getClass().getResource("view/application.css").toExternalForm());
            stage.setScene(scene);

            ApplicationOverviewController controller = loader.getController();
            controller.setMainApp(this);

            stage.show();
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
	public static void main(String[] args) {
		launch(args);
	}
}
